(* open Messages *)
open Word
open Crypto
open Letter

type politician = { sk : Crypto.sk; pk : Crypto.pk } [@@deriving yojson, show]

type state = {
  politician : politician;
  word_store : Store.word_store;
  letter_store : Store.letter_store;
  next_words : word list;
}

(*------------------------------GENERATE WORD------------------------------*)
(* genere toutes les permutations de x dans l *)
let switch_all_positions x l =
  let rec aux prev acc = function
    | [] -> (prev @ [x]) :: acc |> List.rev
    | hd::tl as l -> aux (prev @ [hd]) ((prev @ [x] @ l) :: acc) tl
  in
  aux [] [] l

(* genere tous les permutations possibles de la liste *)
let rec permutations = function
  | [] -> []
  | x::[] -> [[x]] (* one letter in the list *)
  | x::xs -> List.fold_left 
  (fun acc p -> acc @ switch_all_positions x p ) [] (permutations xs)

(* recupere un mot String à partir d'une liste de lettre *)
let get_word = function 
  | [] -> ""
  | list -> String.lowercase_ascii (List.fold_left (fun acc l -> acc ^ String.make 1 l.letter) "" list)

let rec cherche_dico ll dico =
  match ll with
  | [] -> []
  | m::t -> let mot = get_word m in if List.mem mot dico then m else cherche_dico t dico

let rec remove_in_list list elem =
  match list with
  | [] -> []
  | hd::tl -> if hd = elem then remove_in_list tl elem else [hd] @ (remove_in_list tl elem)

(** Creation d'un nouveau mot en regardant s'il existe dans le dictionnaire *)
let rec create_new_word l dico index =
  let length = List.length l in
    if length < 3 (* Dans le dictionnaire, il n'exite pas de mot de moins de 3 lettres *)
    then [] (* Aucun mot ne peut etre créer *)
    else (if length <= index (* Echec permutations de la list entiere et celle du head + sous liste moins un element *)
            then create_new_word (List.tl l) dico 0 (* on supprime le head et on recherche un mot avec le reste *)
            else let p = (if index=0 then permutations l else permutations (remove_in_list l (List.nth l index))) in  
                  let res = cherche_dico p dico in (* p = toute la liste ou la liste - element de l'index*)
                      match res with
                      | [] -> create_new_word l dico (index+1) (* on re-essaye en supprimer l'element de l'index+1*)
                      | _::_ -> res) (* On a trouvé un mot *)
  
(* creattion d'un mot sans vérifié dans le dictionnaire *)
 let create_easy_word l : letter list = let p = permutations l in List.nth p (Random.int (List.length l))

(*------------------------------------------------------------------------*)

(* Verifie que l'auteur n'existe pas dans la liste *)
let rec check_auteurs auteurs auteur =
  match auteurs with
  | a::as_suite -> if a = auteur then false else check_auteurs as_suite auteur
  | [] -> true

(* Algorithme vérifiant que le mot créer est valide ou non *)
(* Ne contenant pas plus d’une lettre venant d’un même client *)
(* Ne contenant que des lettres injectées avec une référence au mot prédécesseur *)
(* Ne contenant pas plus d’une lettre venant d’un même client *)
let check_new_word store {word; level; head; _} = 
    let rec aux letters auteurs = 
      match letters with
      | l::ls -> if ((l.head = head) && (check_auteurs auteurs l.author))  (* && (l.level = level) *)
                  then aux ls (l.author::auteurs) else false 
      | [] -> true
      in let old_head = Consensus.head ~level:(level - 1) store 
        in match old_head with
          | Some w -> let hash_head = hash (Word.to_bigstring w)
                        in if hash_head = head then (aux word []) else false
          | None ->  failwith ("Politicien : not a valid head " ^ __LOC__)

(*------------------------------------------------------------------------*)      

let make_word_on_hash level letters politician head_hash : word =
  let head = head_hash in
  Word.make ~word:letters ~level ~pk:politician.pk ~sk:politician.sk ~head

let make_word_on_blockletters level letters politician head : word =
  let head_hash = hash head in
  make_word_on_hash level letters politician head_hash

let send_new_word st level =
  let dico = Client_utils.list_of_dict "./dict/dict_100000_1_10.txt" in
    let old_head = Consensus.head ~level:(level - 1) st.word_store 
          in (match old_head with
            | Some w -> (let hash_head = w.head in 
                          let l_letters = Store.get_letters st.letter_store hash_head in 
                            let l_word = create_new_word l_letters dico 0 in 
                              match l_word with
                              | _::_ -> (let word = make_word_on_blockletters level l_word st.politician (Word.to_bigstring w) in 
                                          if check_new_word st.word_store word
                                          then (Client_utils.send_some (Messages.Inject_word word); Store.add_word st.word_store word;)
                                          else ())
                              | [] -> ()) (** Dans le cas où create_new_word n'a pas pu créer un mot *)
            | None ->  (failwith ("send_new_word : not a valid head " ^ __LOC__)))

(** Tentative d'envoyer un mot (non valide) au serveur -> FAIL *)
let send_easy_new_word st level =
  let old_head = Consensus.head ~level:(level - 1) st.word_store 
          in (match old_head with
            | Some w -> let letters = create_easy_word (Store.get_letters st.letter_store w.head) 
                          in let easy_word = make_word_on_blockletters level letters st.politician (Word.to_bigstring w) in
                            Client_utils.send_some (Messages.Inject_word easy_word); Store.add_word st.word_store easy_word;
                            (* if check_new_word st.word_store easy_word
                                          then Client_utils.send_some (Messages.Inject_word easy_word)
                                          else () *)
            | None ->  (failwith ("send_easy_new_word : not a valid head " ^ __LOC__))) 
      
let run ?(max_iter = 0) () =

  (* Generate public/secret keys *)
  let (pk, sk) = Crypto.genkeys () in
  
  (* Get initial wordpool *)
  let getpool = Messages.Get_full_wordpool in
  Client_utils.send_some getpool ;
  let wordpool =
    match Client_utils.receive () with
    | Messages.Full_wordpool wordpool -> wordpool
    | _ -> assert false
  in

  (* Generate initial blocktree *)
  let storeOfWords = ref (Store.init_words ()) in
  Store.add_words !storeOfWords wordpool.words ;
  
  Unix.sleep 2;

  (* Get initial letterpool *)
  let getpool = Messages.Get_full_letterpool in 
  Client_utils.send_some getpool ;
  let poolOfLetters =
    match Client_utils.receive () with
      | Messages.Full_letterpool poolOfLetters -> poolOfLetters
      | _ -> assert false
    in

  (* Generate initial local letterpool *)
  let storeOfLetters = ref (Store.init_letters ()) in
  Store.add_letters !storeOfLetters poolOfLetters.letters ;

  (* Create and send first word *)
  let myState = {politician = {sk=sk; pk=pk};
                word_store = !storeOfWords;
                letter_store = !storeOfLetters;
                next_words = [] ;} in 
  
  (* Test d'envoi du mot genesis -> OK 
    Client_utils.send_some (Messages.Inject_word Constants.genesis_word) ;  *)

  (* Test d'envoie d'un mot (non valide) -> FAIL
    send_easy_new_word myState poolOfLetters.current_period ; *)
  
  send_new_word myState poolOfLetters.current_period ; 
  

  (* start listening to server messages *)
  Client_utils.send_some Messages.Listen ;

  (*  main loop *)
  let level = ref poolOfLetters.current_period in 
  let rec loop max_iter cpt=
    if max_iter = 0 then ()
    else (
      ( match Client_utils.receive () with
      | Messages.Next_turn p -> level := p
      | Messages.Inject_letter l -> (
          Store.add_letter !storeOfLetters l;
          Log.log_info "Store a new letter %a@" Letter.pp l;
          if  cpt+1 > 2
            then send_new_word myState !level
            else loop max_iter (cpt+1); )
      | Messages.Inject_word w -> (
          storeOfLetters := (Store.init_letters ()); (*réinitialise le letter pool*)
          (* Ajout du mot si seulement si celui-ci est valide *)
          if check_new_word !storeOfWords w then Store.add_word !storeOfWords w else (failwith ("entré d'un mot non valider dans le serveur"));
          Log.log_info "Store a new word %a@." Word.pp w;)
      | _ -> () );
      loop (max_iter - 1) 0)
    in 
    loop max_iter

let _ =
  let main =
    Random.self_init () ;
    let () = Client_utils.connect () in
    run ~max_iter:(-1) ()
  in
  main
