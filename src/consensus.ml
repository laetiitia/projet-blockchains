open Word
open Constants
open Letter 

let alpha = [('a',1);('b',3);('c',3);('d',2);('e',1);('f',4);('g',2);('h',4);('i',1);('j',8);('k',10);('l',1);('m',2);('n',1);('o',1);('p',3);('q',8);('r',1);('s',1);('t',1);('u',1);('v',4);('w',10);('x',10);('y',10);('z',10)]


let rec get_lettre_score lettre alpha =
  match alpha with
  | [] -> 0;
  | (k,v)::t -> if lettre = k then v else get_lettre_score lettre t


let word_score { word; _ } : int =
  let rec aux res letterlist =
    match letterlist with
    |[] -> res 
    |l::t -> aux (res+(get_lettre_score l.letter alpha)) t
  in
  aux 0 word

(* Permet de choisir le meilleur block valide selon les conditions suivantes:
 - Level
 - Score du mot *)
let fitness st (word : Word.t) =
  let res = ref [] in
    Store.iter_words (fun _ w -> if w.level = word.level then res := !res @ [w]) st;
  if List.length !res = 1
    then word.level 
    (* Si ce n'est pas le seul mot du level on prend celui qui a le meilleur score *)
    else let max_score = List.fold_left 
          (fun s w -> let score = word_score w in if score > s then score else s) 0 !res 
            in if max_score = (word_score word) then word.level else -1

(* Recupere le head du word store en regardant le level *)
let head ?level (st : Store.word_store) =
  if (Store.length st > 0)
  then ( match level with
        | None -> None
        | Some lvl ->
        (let res = ref [(0, genesis_word)] in 
            Store.iter_words (fun _ m -> let (k,_) = List.hd !res in  
              if k < (fitness st m) && lvl = m.level 
                then res := [((fitness st m),m)] ) st;
              let (_,v) = List.hd !res in
                Some v))
  else Some genesis_word